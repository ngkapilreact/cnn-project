'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var Utilities = {
	// DOM classes
	addClass: function addClass(element, className) {
		this.removeClass(element, className);
		element.className = (element.className + ' ' + className).trim();
		this.createDispatchEvent(element, 'classadd', { name: className });
	},
	removeClass: function removeClass(element, className) {
		element.className = (' ' + element.className + ' ').replace(new RegExp(' ' + className + ' ', 'g'), ' ').trim();
		this.createDispatchEvent(element, 'classremove', { name: className });
	},
	toggleClass: function toggleClass(element, className) {
		if (this.hasClass(element, className)) {
			this.removeClass(element, className);
		} else {
			this.addClass(element, className);
		}
	},
	hasClass: function hasClass(element, className) {
		return (' ' + element.className + ' ').indexOf(' ' + className + ' ') >= 0;
	},

	// DOM selection to Array
	querySelectorArray: function querySelectorArray(query, root) {
		return Array.prototype.slice.call((root || document).querySelectorAll(query));
	},

	// Request data
	// Maybe use Promise?
	XMLHttpRequest: function (_XMLHttpRequest) {
		function XMLHttpRequest(_x, _x2, _x3, _x4) {
			return _XMLHttpRequest.apply(this, arguments);
		}

		XMLHttpRequest.toString = function () {
			return _XMLHttpRequest.toString();
		};

		return XMLHttpRequest;
	}(function (url, success, failure, params) {
		if (!success) return;
		if (!failure) failure = function failure(msg, req, data) {
			console.warn(msg);
			console.log(req);
		};
		var request = new XMLHttpRequest();
		request.addEventListener("error", function (e) {
			failure('Error', e);
		});
		request.addEventListener("load", function () {
			if (this.status === 200) success(this.responseText);else failure('Unexptected status: ' + this.status, request);
		});
		request.open(params ? "POST" : "GET", url);
		request.send(params);
	}),

	// takes a list of values and returns the first defined one
	isset: function isset(value, defaultsMultiple) {
		var defaults = Array.prototype.slice.call(arguments);
		for (var i = 0; i < defaults.length; i++) {
			if (typeof defaults[i] !== 'undefined') return defaults[i];
		}
		return false;
	},

	// Scroll an DOM node
	scrollTo: function scrollTo(options) {
		var _this = this;

		if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) !== 'object') return false;
		var element = options.element || this.scrollBodyElement(),
		    tx = this.isset(options.x, options.left, x),
		    ty = this.isset(options.y, options.top, y),
		    x = this.isset(options.startX, element.scrollLeft),
		    x = this.isset(options.x, options.left, x) - x,
		    y = this.isset(options.startY, element.scrollTop),
		    y = this.isset(options.y, options.top, y) - y,
		    e = this.isset(options.easing, function (n) {
			return n;
		}),
		    d = this.isset(options.duration, 400);

		this.createDispatchEvent(e, 'scrollstart');
		var animation = AnimationFrameController.add(function (delta, progress) {
			if (x) element.scrollLeft += delta / d * x;
			if (y) element.scrollTop += delta / d * y;
			if (progress >= d) {
				element.scrollLeft = tx;
				element.scrollTop = ty;
				if (options.callback) options.callback();
				_this.createDispatchEvent(element, 'scrollend');
				return false;
			} else {
				_this.createDispatchEvent(element, 'scrollmove');
			}
		});
		return {
			cancel: function cancel() {
				AnimationFrameController.remove(animation);
				this.createDispatchEvent(element, 'scrollend');
				this.id = false;
			},
			id: animation };
	},

	// FAQs
	scrollBodyElement: function scrollBodyElement() {
		return this.isInternetExplorer() || this.isFireFox() ? document.documentElement : document.body;
	},
	isInternetExplorer: function isInternetExplorer() {
		return !!(navigator.userAgent.toLowerCase().indexOf('msie') >= 0 || navigator.userAgent.toLowerCase().indexOf('trident') >= 0);
	},
	isFireFox: function isFireFox() {
		return !!(navigator.userAgent.toLowerCase().indexOf('firefox') >= 0);
	},
	inViewport: function inViewport(element) {
		var bb = element.getBoundingClientRect();
		return bb.top >= -bb.height && bb.top <= window.innerHeight ? bb : false;
	},

	// Events
	createDispatchEvent: function createDispatchEvent(object, type) {
		var detail = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

		this.dispatchEvent(object, this.createCustomEvent(type, detail));
	},
	createCustomEvent: function createCustomEvent(type) {
		var detail = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

		try {
			return new CustomEvent(type, { detail: detail });
		} catch (e) {
			var event = document.createEvent("CustomEvent");
			event.initCustomEvent(type, true, true, { detail: detail });
			return event;
		}
	},
	dispatchEvent: function dispatchEvent(object, event) {
		if (object.dispatchEvent) {
			object.dispatchEvent(event);
		} else if (object.fireEvent) {
			object.fireEvent("on" + event.type, event);
		}
	}
};