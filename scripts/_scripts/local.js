'use strict';

// Creates social media link and markup
Utilities.querySelectorArray(['#social']).forEach(function (wrapper) {
    return createShareElement({
        wrapper: wrapper
    });
});
Utilities.querySelectorArray(['#social-share']).forEach(function (wrapper) {
    return createShareElement({
        wrapper: wrapper
    });
});

var app = {};

$(document).ready(function () {
    $('body').on('click', '.nav-trigger.action, .nav-close', function () {
        $('body').toggleClass('nav-active');
    });
});

var $activeslide = $('.activeslide'),
    $slides = $('.module'),
    $sliderNext = $('.next-arrow'),
    $sliderPrev = $('.prev-arrow'),
    $currentSlide = $slides.first(),
    isAnimating = false,
    isSliderAnimating = false,
    isSliderEnter = false,
    currentIndex = 0;

var tlSlidersEnter = [];
var tlSlidersLeave = [];
var tlSlidersAnimationEnter = [];

var keyCodes = {
    UP: 38,
    DOWN: 40
};

$('.counter-bottom').html($slides.length - 2);
app = {
    sliderLeaveEnd: false,
    sliderLeaveRevEnd: false,
    sliderEnterEnd: false,
    sliderEnterRevEnd: false
    //  var tlSliderFirst = new TimelineMax();
    //    tlSliderFirst
    //            .fromTo($currentSlide.find('.video-overlay'), 1, {autoAlpha: 0, y: 100}, {autoAlpha: 1, y: 0, ease: Linear.ease})
    //            .to($currentSlide.find('.video-overlay'), 1, {autoAlpha: 0, ease: Linear.ease}, '3');

};$slides.each(function (index) {
    var slideAnimation = $(this).data('animation'),
        slideAnimationLeave = $(this).data('animation-leave'),
        sliderType = $(this).data('slider-type'),
        tlSliderAnimationEnter = new TimelineMax({
        onComplete: onSlideChangeEnd
    }),
        tlSliderEnter = new TimelineMax({
        onComplete: onSliderEnterEnd,
        onReverseComplete: onSliderEnterRevEnd
    }),
        tlSliderLeave = new TimelineMax({
        onReverseComplete: onSliderLeaveRevEnd,
        onComplete: onSliderLeaveEnd
    });
    // tlOverlay = new TimelineMax({});

    // tlOverlay.to(".fifth-overlay", 1, {ease: fadeOut});

    tlSliderAnimationEnter.fromTo($(this).find('.color-bg-block.left'), 0.4, {
        width: 0
    }, {
        width: "100%",
        ease: Linear.ease
    }).fromTo($(this).find('.media-overlay.fadeIn'), 0.2, {
        autoAlpha: 0
    }, {
        autoAlpha: 1,
        ease: Power1.easeInOut
    }).fromTo($(this).find('.color-bg-block.right'), 0.4, {
        right: "-100%"
    }, {
        right: 0,
        ease: Linear.ease
    }).fromTo($(this).find('.fadeInUp'), 0.4, {
        autoAlpha: 0,
        y: "10"
    }, {
        autoAlpha: 1,
        y: '0',
        ease: Power1.easeInOut
    }).fromTo($(this).find('.content.fadeIn'), 0.4, {
        autoAlpha: 0
    }, {
        autoAlpha: 1,
        ease: Power1.easeInOut
    }, "+=0.5").fromTo($(this).find('.baby-blue.fadeIn'), 0.1, {
        autoAlpha: 0
    }, {
        autoAlpha: 1,
        ease: Linear.easeInOut
    }).fromTo($(this).find('.text.fadeIn'), 0.1, {
        autoAlpha: 0
    }, {
        autoAlpha: 1,
        ease: Linear.easeInOut
    }).fromTo($(this).find('.lines span'), 0.4, {
        width: 0
    }, {
        width: "100%",
        ease: Linear.ease
    }).reverse();

    /*
     *   If there's a  slider slide
     * */
    if (slideAnimation === "slide") {
        tlSliderEnter.fromTo($(this), 1, {
            xPercent: 100
        }, {
            xPercent: 0,
            ease: Power4.easeInOut
        }).reverse();
        if (slideAnimationLeave === "fade") {
            tlSliderLeave.fromTo($(this), 1, {
                autoAlpha: 1
            }, {
                autoAlpha: 0,
                ease: Power4.easeInOut
            }).reverse();
        } else {
            tlSliderLeave.fromTo($(this), 1, {
                xPercent: 0
            }, {
                xPercent: -100,
                ease: Power4.easeInOut
            }).reverse();
        }
    }
    /*
     *   If there's a  slider fade
     * */
    if (slideAnimation === "fade") {
        tlSliderEnter.fromTo($(this), 1, {
            autoAlpha: 0
        }, {
            autoAlpha: 1,
            ease: Power4.easeInOut
        }).reverse();
        if (slideAnimationLeave === "slide") {
            tlSliderLeave.fromTo($(this), 1, {
                xPercent: 0
            }, {
                xPercent: -100,
                ease: Power4.easeInOut
            }).reverse();
        } else {
            tlSliderLeave.fromTo($(this), 1, {
                autoAlpha: 1
            }, {
                autoAlpha: 0,
                ease: Power4.easeInOut
            }).reverse();
        }
    }

    tlSlidersEnter.push(tlSliderEnter);
    tlSlidersLeave.push(tlSliderLeave);
    tlSlidersAnimationEnter.push(tlSliderAnimationEnter);
});

/*
 *   Adding event listeners
 * */

$sliderNext.on("click", goToNextSlide);
$sliderPrev.on("click", goToPrevSlide);

$(document).on('keyup.slider', function (evt) {
    if (evt.keyCode == 37) {
        // LEFT
        $($sliderPrev).trigger('click');
    } else if (evt.keyCode == 39) {
        // RIGHT
        $($sliderNext).trigger('click');
    }
});

if (!$('body').hasClass("nav-active")) {
    $(window).on("mousewheel DOMMouseScroll", onMouseWheel);
    $(document).on("keydown", onKeyDown);
}

/*
 *   If there's a next slide, slide to it
 * */
function goToPrevSlide() {
    if ($currentSlide.prev().length) {
        goToSlide($currentSlide.prev(), $currentSlide);
    }
}

/*
 *   If there's a next slide, slide to it
 * */
function goToNextSlide() {
    if ($currentSlide.next().length) {
        goToSlide($currentSlide.next(), $currentSlide);
    }
}

/*
 *   Actual transition between slides
 * */
function goToSlide($slide, $lastCurrent) {
    //If the slides are not changing and there's such a slide
    if (!isAnimating && $slide.length) {

        //setting animating flag to true
        isAnimating = true;
        $currentSlide = $slide;

        // Change the index
        currentIndex = $currentSlide.index();

        if (currentIndex > $lastCurrent.index()) {
            //Next
            tlSlidersEnter[currentIndex].reversed(false);
            tlSlidersLeave[currentIndex - 1].reversed(false);

            progressBar();
        } else {
            //Previous
            tlSlidersLeave[currentIndex].reversed(true);
            tlSlidersEnter[currentIndex + 1].reversed(true);

            progressBar();
        }

        //Definig slide status
        //TweenLite.to($slides.filter(".active"), 0.1, {className: "-=active"});
        TweenLite.to($slides.filter($currentSlide), 0.1, {
            className: "+=active"
        });
    }
}

function onSliderEnterEnd() {
    tlSlidersAnimationEnter[currentIndex].reversed(false);
}

function onSliderLeaveEnd() {
    tlSlidersAnimationEnter[currentIndex - 1].reversed(true);
}

function onSliderEnterRevEnd() {
    tlSlidersAnimationEnter[currentIndex].reversed(false);
}

function onSliderLeaveRevEnd() {
    tlSlidersAnimationEnter[currentIndex + 1].reversed(true);
}

/*
 *   Once the sliding is finished, we need to restore "isAnimating" flag.
 *   You can also do other things in this function, such as changing page title
 * */
function onSlideChangeEnd() {
    isAnimating = false;
    currentIndex = $currentSlide.index();
    // Reverse the timeline for the previous slide
    //tlSliders[currentIndex].reversed(true).progress(0);

    // Play the timeline for the current slide
    //tlSliders[currentIndex].reversed(false);
}

/*
 *   Getting the pressed key. Only if it's up or down arrow, we go to prev or next slide and prevent default behaviour
 *   This way, if there's text input, the user is still able to fill it
 * */
function onKeyDown(event) {

    var PRESSED_KEY = event.keyCode;

    if (PRESSED_KEY == keyCodes.UP) {
        goToPrevSlide();
        event.preventDefault();
    } else if (PRESSED_KEY == keyCodes.DOWN) {
        goToNextSlide();
        event.preventDefault();
    }
}

/*
 *   When user scrolls with the mouse, we have to change slides
 * */
function onMouseWheel(event) {
    //Normalize event wheel delta
    var delta = event.originalEvent.wheelDelta / 30 || -event.originalEvent.detail;

    //If the user scrolled up, it goes to previous slide, otherwise - to next slide
    if (delta < -1) {
        goToNextSlide();
    } else if (delta > 1) {
        goToPrevSlide();
    }
    event.preventDefault();
}

/*
 *   Progress bar
 * */
function progressBar() {
    var percentage = 0;
    var statusWidth = 0;
    percentage = (currentIndex + 1) / $slides.length;
    statusWidth = $(window).width() * percentage;
    $('.progress-bar-fill').css('width', statusWidth);

    if (currentIndex < 10) {
        $('.counter-top').html("0" + currentIndex);
    } else {
        $('.counter-top').html(currentIndex);
    }
    if (currentIndex > 0) {
        $('body').addClass('slider-start');
        $('.nav-trigger').addClass('action');
    } else {
        $('body').removeClass('slider-start');
        $('.nav-trigger').removeClass('action');
    }
    // console.log(currentIndex)
    // console.log($slides.length)
    if (currentIndex + 1 === $slides.length) {
        $('body').addClass('slider-end');
    } else {
        $('body').removeClass('slider-end');
    }
}

// lazy-loading all the images
// if viewport changes, page need to be refreshed
document.addEventListener("DOMContentLoaded", function () {
    var lazyloadImages = document.querySelectorAll("img.lazy");
    var lazyloadThrottleTimeout;

    function lazyload() {
        if (lazyloadThrottleTimeout) {
            clearTimeout(lazyloadThrottleTimeout);
        }

        lazyloadThrottleTimeout = setTimeout(function () {
            var scrollTop = window.pageYOffset;
            lazyloadImages.forEach(function (img) {
                if (img.offsetTop < window.innerHeight + scrollTop) {
                    img.src = img.dataset.src;
                    img.classList.remove('lazy');
                }
                console.log("Lazy load triggered");
            });
            if (lazyloadImages.length == 0) {
                document.removeEventListener("scroll", lazyload);
                window.removeEventListener("resize", lazyload);
                window.removeEventListener("orientationChange", lazyload);
            }
        }, 20);
    }

    document.addEventListener("scroll", lazyload);
    window.addEventListener("resize", lazyload);
    window.addEventListener("orientationChange", lazyload);
    console.log("Lazy load initiated");
});

// fade out after 3 sec for first video
if ($(".module-video").visible(true)) {
    setTimeout(function () {
        console.log("first video visible");
        $(".video-content-title-1").fadeOut();
        $('.video-sound-wrapper-1').fadeOut();
        $('.video-overlay-1').fadeOut();
    }, 3000);
}

// define video /mute property
var vid = document.querySelectorAll('video');

// get src of video for changing media query dynamically
for (var ids in vid) {
    var splitter = [];
    for (var j = 0; j < vid.length; j++) {
        splitter[j] = vid[ids].src.split("/");
        // console.log("Breaked src is ", splitter[j]);

        // check device media 
        // change the value of splitter to exact original url at last
        var mq = window.matchMedia("(max-width: 425px)");
        // console.log("value of media uery is:", mq );

        // will change src to nobiles
        if (mq.matches) {
            splitter[6] == 'mob';
            console.log("in mobile");
            splitter[j].join("/");
        }

        // for desktop sources
        else {
                splitter[6] == 'desk';
                console.log("In desktop");
                splitter[j].join("/");
            }
    }
}

function muted() {
    for (var i = 0; i < vid.length; i++) {

        if (vid[i].muted) {
            vid[i].muted = false;
            $("#mute-icon").show();
            $("#unmute-icon").hide();
            $(".video-sound-wrapper").hide();
        } else {
            vid[i].muted = true;
            $("#mute-icon").hide();
            $("#unmute-icon").show();
        }
    }
};

// Data-saver detection (applicable in mobile only / for desktop no longer supported)
if ('connection' in navigator) {
    if (navigator.connection.saveData) {
        vid.autoplay = false;
    } else {
        vid.autoplay = true;
    }
}