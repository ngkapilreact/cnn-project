'use strict';

var AnimationFrameController = function () {

	/* Private variables */

	var functions = [],
	    animationFrame = false,
	    time = -1,
	    id = 0;

	/* Main function loop */

	function loop(newTime) {

		for (var f = 0; f < functions.length; f++) {
			if (functions[f].time === -1) {
				functions[f].time = newTime;
			} else {
				var result = functions[f].handler(newTime - time, newTime - functions[f].time);
				if (result === false) {
					AF.remove(functions[f].id);
					f--;
				}
			}
		}

		time = newTime;

		if (animationFrame && functions.length) {
			animationFrame = window.requestAnimationFrame(loop);
		} else {
			AF.stop();
		}
	}

	/* Interface */

	var AF = {
		start: function start() {
			animationFrame = window.requestAnimationFrame(loop);
		},
		stop: function stop() {
			animationFrame = !!(window.cancelAnimationFrame(animationFrame) && false);
		},

		get paused() {
			return !!animationFrame;
		},
		autostart: true,
		add: function add(handler) {
			id++;
			functions.unshift({
				'time': -1,
				'handler': handler,
				'id': id
			});
			if (this.autostart && functions.length) {
				this.start();
			}
			return id;
		},
		remove: function remove(handlerOrId) {
			var returnFunction;
			for (var f = 0; f < functions.length; f++) {
				if (functions[f].id === handlerOrId || functions[f].handler === handlerOrId) {
					returnFunction = functions[f].handler;
					functions.splice(f, 1);
					f--;
				}
			}
			if (this.autostart && functions.length === 0) {
				this.stop();
			}
			return returnFunction;
		},
		debug: function debug() {
			return {
				'functions': functions.slice(),
				'animationFrame': animationFrame || false,
				'time': time
			};
		}
	};

	return AF;
}();
'use strict';

function applyParallax() {
	var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '*[data-parallax]';
	var attribute = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'data-parallax';


	// Updated version on 24/01/2017

	// data-parallax: contains range and unit to cycle through
	// data-parallax-css: contains the CSS attribute the calculation needs to be applied to
	// data-parallax-css-value: value with x,y (in that order), use <%> as indicator
	// data-parallax-limit: limit of scroll application

	var cssUnitList = ['px', 'em', 'vw', 'vh', 'vmin', 'vmax', 'rem', '%'];
	var easingFunctions = {
		linear: function linear(v) {
			return v;
		},
		cubic: function cubic(v) {
			return v * v;
		}
	};

	Utilities.querySelectorArray('[data-parallax]').forEach(function (parallax) {

		var range = (parallax.getAttribute('data-parallax') || '40->60,40->60').split(',').map(function (v) {
			var values = v.split('->').slice(0, 2).map(parseFloat);
			return {
				unit: cssUnitList.find(function (u) {
					return v.indexOf(u) > 0;
				}) || '%',
				difference: values[1] - values[0],
				base: values[0]
			};
		});
		var attribute = parallax.getAttribute('data-parallax-css') || 'transform';
		var value = parallax.getAttribute('data-parallax-css-value') || 'translate(<%>,<%>)';
		var limit = function () {
			var limits = (parallax.getAttribute('data-parallax-limit') || '-1,1').split(',').map(parseFloat);
			return function (value) {
				value = value < limits[0] ? limits[0] : value > limits[1] ? limits[1] : value;
				return (value - limits[0]) / (limits[1] - limits[0]);
			};
		}();
		var easing = easingFunctions[parallax.getAttribute('data-parallax-easing')] || easingFunctions.cubic;

		// Convert attribute to appropriate CSS value where necessary
		var cssEvaluate = attribute.split('');
		var cssAttrConvert = cssEvaluate.indexOf('-');
		while (cssAttrConvert >= 0) {
			cssEvaluate.splice(cssAttrConvert, 2, cssEvaluate[cssAttrConvert + 1].toUpperCase());
			cssAttrConvert = cssEvaluate.indexOf('-');
		}
		attribute = cssEvaluate.join('');

		var scroll = onScroll || new EventThrottler(document, 'scroll');

		scroll.addHandler(function () {
			var bounds = Utilities.inViewport(parallax);
			if (bounds) {
				var height = bounds.height < innerHeight ? innerHeight : bounds.height;
				var delta = easing(limit(-bounds.top / height));
				parallax.style[attribute] = range.map(function (v) {
					return (v.base + delta * v.difference).toFixed(2) + v.unit;
				}).reduce(function (r, v) {
					return r.replace('<%>', v);
				}, value);
			}
		});
	});
}
'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EventThrottler = function EventThrottler(object, eventName) {
	var _this = this;

	var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
	    _ref$timing = _ref.timing,
	    timing = _ref$timing === undefined ? 1000 : _ref$timing,
	    _ref$firstEvent = _ref.firstEvent,
	    firstEvent = _ref$firstEvent === undefined ? false : _ref$firstEvent,
	    _ref$autostart = _ref.autostart,
	    autostart = _ref$autostart === undefined ? true : _ref$autostart,
	    _ref$preventDefault = _ref.preventDefault,
	    preventDefault = _ref$preventDefault === undefined ? false : _ref$preventDefault,
	    _ref$stopImmediatePro = _ref.stopImmediatePropagation,
	    stopImmediatePropagation = _ref$stopImmediatePro === undefined ? false : _ref$stopImmediatePro;

	_classCallCheck(this, EventThrottler);

	var event = function () {
		var E = false;
		return function () {
			var event = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

			if (event && preventDefault) event.preventDefault();
			if (event && stopImmediatePropagation) event.stopImmediatePropagation();
			if (event && (!E || !firstEvent)) {
				if (typeof event.constructor === 'function') {
					E = new event.constructor(event.type, event);
				} else {
					E = event;
				}
				return E;
			} else if (event === false) {
				var EC = E;
				E = false;
				return EC;
			}
		};
	}();
	var timeout = void 0;
	var handlers = [];
	var self = this;
	var AFwrapper = typeof AnimationFrameController !== 'undefined' ? function () {
		return _this.fire();
	} : false;

	object.addEventListener(eventName, event);

	this.fire = function () {
		var forced = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

		var e = event();
		if (forced || e) handlers.forEach(function (handler) {
			return handler(forced || e);
		});
	};
	this.start = function () {
		if (AFwrapper) {
			AnimationFrameController.add(AFwrapper);
		} else {
			timeout = setInterval(function () {
				self.fire();
			}, timing);
		}
	};
	this.stop = function () {
		if (AFwrapper) {
			animationFrameController.remove(AFwrapper);
		} else {
			clearInterval(timeout);
		}
	};
	this.removeHandler = function (handler) {
		var index = -1;
		while ((index = handlers.indexOf(handler)) >= 0) {
			handlers.splice(index, 1);
		}
		return handler;
	};
	this.addHandler = function (handler) {
		if (handlers.indexOf(handler) < 0) {
			handlers.push(handler);
		}
		return handler;
	};

	if (autostart) this.start();
};
"use strict";

/*
 * Construct Share v3.0 // 08 10 2016
 * constructShare([div:Node, options:Object]);
 * -> div:Node.chilNodes([a:Node, ...]);
 */

var createShareElement = function () {

	var networkUrls = {
		linkedin: "http://www.linkedin.com/shareArticle?mini=true&url={articleUrl}",
		facebook: "https://www.facebook.com/sharer/sharer.php?u={articleUrl}",
		twitter: "https://twitter.com/home?status={articleUrl}",
		email: "mailto:?subject={subject}&body={articleUrl}",
		googleplus: "https://plus.google.com/share?url={articleUrl}"
	};

	return function shareElements(_ref) {
		var _ref$url = _ref.url,
		    url = _ref$url === undefined ? location.href : _ref$url,
		    _ref$type = _ref.type,
		    type = _ref$type === undefined ? 'a' : _ref$type,
		    _ref$className = _ref.className,
		    className = _ref$className === undefined ? 'share__' : _ref$className,
		    _ref$networks = _ref.networks,
		    networks = _ref$networks === undefined ? ['facebook', 'twitter', 'email', 'linkedin'] : _ref$networks,
		    _ref$title = _ref.title,
		    title = _ref$title === undefined ? encodeURIComponent('Shared: ' + document.title) : _ref$title,
		    _ref$wrapper = _ref.wrapper,
		    wrapper = _ref$wrapper === undefined ? 'div' : _ref$wrapper,
		    _ref$urls = _ref.urls,
		    urls = _ref$urls === undefined ? networkUrls : _ref$urls;


		return networks.reduce(function (wrapper, network) {
			var link = document.createElement(type);
			link.className = className + network;
			link.href = urls[network].replace('{articleUrl}', url).replace('{subject}', title);
			link.target = "_blank";
			link.textContent = 'share on ' + network;
			wrapper.appendChild(link);
			return wrapper;
		}, typeof wrapper === 'string' ? document.createElement(wrapper) : wrapper);
	};
}();
'use strict';

function replaceSVG() {
	var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '[data-svg]';
	var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'DISPATCH_EVENT';


	if (!!(document.createElementNS && document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect)) {

		var all = Utilities.querySelectorArray(query);
		var done = [];

		// Replace all placeholder with SVGs
		all.forEach(function (element) {

			// Make sure the important layout attributes are retained when changing the element
			var id = element.id;
			var classes = element.className;
			var source = element.getAttribute('data-svg');

			function end() {
				if (done.length === all.length && callback === 'DISPATCH_EVENT') {
					if (window.dispatchEvent) {
						window.dispatchEvent(new CustomEvent('svgreplaced', { detail: {
								query: query, done: done, success: true
							} }));
					}
				} else if (done.length === all.length && typeof callback == 'function') {
					callback(done);
				}
			}

			Utilities.XMLHttpRequest(source, function (text) {
				var holder = document.createElement('div');
				holder.innerHTML = text;
				var svg = holder.querySelector('svg');
				if (svg) {
					svg.setAttribute('id', id);
					svg.setAttribute('class', classes);
					svg.setAttribute('data-svg-source', source);
					element.parentNode.insertBefore(svg, element);
					element.parentNode.removeChild(element);
					done.push(svg);
				} else {
					done.push(element);
				}
				end();
			}, function () {
				done.push(element);
				end();
			});
		});
	} else {
		window.dispatchEvent(new CustomEvent('svgreplaced', { detail: {
				query: query, done: [], success: false
			} }));
	}
}