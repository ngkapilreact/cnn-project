function applyParallax( query = '*[data-parallax]', attribute = 'data-parallax' ){
	
	// Updated version on 24/01/2017
	
	// data-parallax: contains range and unit to cycle through
	// data-parallax-css: contains the CSS attribute the calculation needs to be applied to
	// data-parallax-css-value: value with x,y (in that order), use <%> as indicator
	// data-parallax-limit: limit of scroll application
	
	const cssUnitList = ['px','em','vw','vh','vmin','vmax','rem','%'];
	const easingFunctions = {
		linear: v => v,
		cubic: v =>  v * v
	}

	Utilities.querySelectorArray('[data-parallax]').forEach(parallax => {
		
		var range = (parallax.getAttribute('data-parallax') || '40->60,40->60').split(',').map(v => {
			let values = v.split('->').slice(0, 2).map(parseFloat);
			return {
				unit: cssUnitList.find(u => v.indexOf(u) > 0) || '%',
				difference: values[1] - values[0],
				base: values[0]
			}
		});
		var attribute = parallax.getAttribute('data-parallax-css') || 'transform';
		var value = parallax.getAttribute('data-parallax-css-value') || 'translate(<%>,<%>)';
		var limit = function(){
			var limits = (parallax.getAttribute('data-parallax-limit') || '-1,1').split(',').map( parseFloat );
			return value => {
				value = value < limits[0] ? limits[0] : (value > limits[1] ? limits[1] : value);
				return (value - limits[0]) / (limits[1] - limits[0]);
			}
		}();
		var easing = easingFunctions[ parallax.getAttribute('data-parallax-easing') ] || easingFunctions.cubic;
		
		// Convert attribute to appropriate CSS value where necessary
		var cssEvaluate = attribute.split('');
		var cssAttrConvert = cssEvaluate.indexOf('-');
		while( cssAttrConvert >= 0 ){
			cssEvaluate.splice( cssAttrConvert, 2, cssEvaluate[ cssAttrConvert + 1 ].toUpperCase() );
			cssAttrConvert = cssEvaluate.indexOf('-');
		}
		attribute = cssEvaluate.join('');
		
		var scroll = onScroll || new EventThrottler(document, 'scroll');
		
		scroll.addHandler(function(){
			var bounds = Utilities.inViewport( parallax );
			if( bounds ){
				let height = bounds.height < innerHeight ? innerHeight : bounds.height;
				let delta = easing( limit( -bounds.top / height ) );
				parallax.style[attribute] = range.map(v => {
					return (v.base + delta * v.difference).toFixed(2) + v.unit;
				}).reduce((r,v) => {
					return r.replace('<%>', v);
				}, value);
			}
		});
		
	});
	
}