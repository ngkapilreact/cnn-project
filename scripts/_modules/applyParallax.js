'use strict';

function applyParallax() {
	var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '*[data-parallax]';
	var attribute = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'data-parallax';


	// Updated version on 24/01/2017

	// data-parallax: contains range and unit to cycle through
	// data-parallax-css: contains the CSS attribute the calculation needs to be applied to
	// data-parallax-css-value: value with x,y (in that order), use <%> as indicator
	// data-parallax-limit: limit of scroll application

	var cssUnitList = ['px', 'em', 'vw', 'vh', 'vmin', 'vmax', 'rem', '%'];
	var easingFunctions = {
		linear: function linear(v) {
			return v;
		},
		cubic: function cubic(v) {
			return v * v;
		}
	};

	Utilities.querySelectorArray('[data-parallax]').forEach(function (parallax) {

		var range = (parallax.getAttribute('data-parallax') || '40->60,40->60').split(',').map(function (v) {
			var values = v.split('->').slice(0, 2).map(parseFloat);
			return {
				unit: cssUnitList.find(function (u) {
					return v.indexOf(u) > 0;
				}) || '%',
				difference: values[1] - values[0],
				base: values[0]
			};
		});
		var attribute = parallax.getAttribute('data-parallax-css') || 'transform';
		var value = parallax.getAttribute('data-parallax-css-value') || 'translate(<%>,<%>)';
		var limit = function () {
			var limits = (parallax.getAttribute('data-parallax-limit') || '-1,1').split(',').map(parseFloat);
			return function (value) {
				value = value < limits[0] ? limits[0] : value > limits[1] ? limits[1] : value;
				return (value - limits[0]) / (limits[1] - limits[0]);
			};
		}();
		var easing = easingFunctions[parallax.getAttribute('data-parallax-easing')] || easingFunctions.cubic;

		// Convert attribute to appropriate CSS value where necessary
		var cssEvaluate = attribute.split('');
		var cssAttrConvert = cssEvaluate.indexOf('-');
		while (cssAttrConvert >= 0) {
			cssEvaluate.splice(cssAttrConvert, 2, cssEvaluate[cssAttrConvert + 1].toUpperCase());
			cssAttrConvert = cssEvaluate.indexOf('-');
		}
		attribute = cssEvaluate.join('');

		var scroll = onScroll || new EventThrottler(document, 'scroll');

		scroll.addHandler(function () {
			var bounds = Utilities.inViewport(parallax);
			if (bounds) {
				var height = bounds.height < innerHeight ? innerHeight : bounds.height;
				var delta = easing(limit(-bounds.top / height));
				parallax.style[attribute] = range.map(function (v) {
					return (v.base + delta * v.difference).toFixed(2) + v.unit;
				}).reduce(function (r, v) {
					return r.replace('<%>', v);
				}, value);
			}
		});
	});
}