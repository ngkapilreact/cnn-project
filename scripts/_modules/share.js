"use strict";

/*
 * Construct Share v3.0 // 08 10 2016
 * constructShare([div:Node, options:Object]);
 * -> div:Node.chilNodes([a:Node, ...]);
 */

var createShareElement = function () {

	var networkUrls = {
		linkedin: "http://www.linkedin.com/shareArticle?mini=true&url={articleUrl}",
		facebook: "https://www.facebook.com/sharer/sharer.php?u={articleUrl}",
		twitter: "https://twitter.com/home?status={articleUrl}",
		email: "mailto:?subject={subject}&body={articleUrl}",
		googleplus: "https://plus.google.com/share?url={articleUrl}"
	};

	return function shareElements(_ref) {
		var _ref$url = _ref.url,
		    url = _ref$url === undefined ? location.href : _ref$url,
		    _ref$type = _ref.type,
		    type = _ref$type === undefined ? 'a' : _ref$type,
		    _ref$className = _ref.className,
		    className = _ref$className === undefined ? 'share__' : _ref$className,
		    _ref$networks = _ref.networks,
		    networks = _ref$networks === undefined ? ['facebook', 'twitter', 'email', 'linkedin'] : _ref$networks,
		    _ref$title = _ref.title,
		    title = _ref$title === undefined ? encodeURIComponent('Shared: ' + document.title) : _ref$title,
		    _ref$wrapper = _ref.wrapper,
		    wrapper = _ref$wrapper === undefined ? 'div' : _ref$wrapper,
		    _ref$urls = _ref.urls,
		    urls = _ref$urls === undefined ? networkUrls : _ref$urls;


		return networks.reduce(function (wrapper, network) {
			var link = document.createElement(type);
			link.className = className + network;
			link.href = urls[network].replace('{articleUrl}', url).replace('{subject}', title);
			link.target = "_blank";
			link.textContent = 'share on ' + network;
			wrapper.appendChild(link);
			return wrapper;
		}, typeof wrapper === 'string' ? document.createElement(wrapper) : wrapper);
	};
}();