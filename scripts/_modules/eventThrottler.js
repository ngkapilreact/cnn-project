'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EventThrottler = function EventThrottler(object, eventName) {
	var _this = this;

	var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
	    _ref$timing = _ref.timing,
	    timing = _ref$timing === undefined ? 1000 : _ref$timing,
	    _ref$firstEvent = _ref.firstEvent,
	    firstEvent = _ref$firstEvent === undefined ? false : _ref$firstEvent,
	    _ref$autostart = _ref.autostart,
	    autostart = _ref$autostart === undefined ? true : _ref$autostart,
	    _ref$preventDefault = _ref.preventDefault,
	    preventDefault = _ref$preventDefault === undefined ? false : _ref$preventDefault,
	    _ref$stopImmediatePro = _ref.stopImmediatePropagation,
	    stopImmediatePropagation = _ref$stopImmediatePro === undefined ? false : _ref$stopImmediatePro;

	_classCallCheck(this, EventThrottler);

	var event = function () {
		var E = false;
		return function () {
			var event = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

			if (event && preventDefault) event.preventDefault();
			if (event && stopImmediatePropagation) event.stopImmediatePropagation();
			if (event && (!E || !firstEvent)) {
				if (typeof event.constructor === 'function') {
					E = new event.constructor(event.type, event);
				} else {
					E = event;
				}
				return E;
			} else if (event === false) {
				var EC = E;
				E = false;
				return EC;
			}
		};
	}();
	var timeout = void 0;
	var handlers = [];
	var self = this;
	var AFwrapper = typeof AnimationFrameController !== 'undefined' ? function () {
		return _this.fire();
	} : false;

	object.addEventListener(eventName, event);

	this.fire = function () {
		var forced = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

		var e = event();
		if (forced || e) handlers.forEach(function (handler) {
			return handler(forced || e);
		});
	};
	this.start = function () {
		if (AFwrapper) {
			AnimationFrameController.add(AFwrapper);
		} else {
			timeout = setInterval(function () {
				self.fire();
			}, timing);
		}
	};
	this.stop = function () {
		if (AFwrapper) {
			animationFrameController.remove(AFwrapper);
		} else {
			clearInterval(timeout);
		}
	};
	this.removeHandler = function (handler) {
		var index = -1;
		while ((index = handlers.indexOf(handler)) >= 0) {
			handlers.splice(index, 1);
		}
		return handler;
	};
	this.addHandler = function (handler) {
		if (handlers.indexOf(handler) < 0) {
			handlers.push(handler);
		}
		return handler;
	};

	if (autostart) this.start();
};