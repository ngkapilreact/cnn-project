const gulp = require('gulp');
const watch = require('gulp-watch');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const minify = require('gulp-minify');
const uglify = require('gulp-uglify');
//const compass = require('gulp-compass');
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();

var reload = browserSync.reload;


function swallowError(error) {
    console.log(error.toString())
    this.emit('end')
}
/**
 * Task: `browser-sync`.
 *
 */
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        open: true,
    });
});

gulp.task('html', function () {
    gulp.src('*.html')
            .pipe(browserSync.stream());
});

function plumberError(error) {
    console.log('[' + error.plugin + ']: ' + error.message);
    this.emit('end');
}

function js_folder(name, files = false) {
    return gulp.src(
            files || ['./scripts/_maximised/' + name + '/*.js']
            ).pipe(plumber(plumberError)).pipe(
            babel({
                presets: ['es2015-ie']
            })
            )
//            .pipe(minify({
//        ext: {min: '.min.js'},
//        noSource: true
//    }))
//            .pipe(uglify())
            .pipe(
            gulp.dest('./scripts/_' + name + '/')
            ).pipe(
            concat(name + '.all.js')
            ).pipe(
            gulp.dest('./scripts/')
            );
}

gulp.task('js-all', function () {
    return gulp.src(
            [
                './scripts/utilities.all.js',
                './scripts/modules.all.js',
                './scripts/components.all.js',
                './scripts/scripts.all.js',
            ]
            ).pipe(
            plumber(plumberError)
            ).pipe(
            concat('all.min.js')
            )
//            .pipe( uglify())
            .pipe(
            gulp.dest('./scripts/')
            )
});

gulp.task('js-components', function () {
    return js_folder('components');
});

gulp.task('js-utilities', function () {
    return js_folder('utilities', [
        './node_modules/babel-polyfill/dist/polyfill.min.js',
        './scripts/_maximised/utilities/_core.js',
        './scripts/_maximised/utilities/*.js'
    ]);
});

gulp.task('js-scripts', function () {
    return js_folder('scripts');
});

gulp.task('js-modules', function () {
    return js_folder('modules');
});

gulp.task('js', function () {
    return [
        gulp.start('js-scripts'),
        gulp.start('js-modules'),
        gulp.start('js-utilities'),
        gulp.start('js-components')
    ];
});

//gulp.task('css', function () {
//    return gulp.src([
//        './sass/*.scss'
//    ]).pipe(plumber(plumberError)).pipe(
//            compass({
//                css: './css/',
//                sass: './sass/',
//                image: './images/'
//            })
//            ).pipe(
//            autoprefixer({browsers: [
//                    'last 2 version',
//                    'safari 5',
//                    'ie 7', 'ie 8', 'ie 9',
//                    'opera 12.1',
//                    'ios 6', 'android 4'
//                ]})
//            ).pipe(
//            cleanCSS()
//            ).pipe(
//            gulp.dest('./css/')
//            )
//            .pipe(browserSync.stream());
//});

gulp.task('sass', function () {
    return gulp.src('./sass/*.scss')
            .pipe(sass())
            .on('error', swallowError)
            .pipe(autoprefixer({
                browsers: [
                    'last 2 version',
                    'safari 5',
                    'ie 7', 'ie 8', 'ie 9',
                    'opera 12.1',
                    'ios 6', 'android 4'
                ],
                cascade: false
            }))
            .pipe(cleanCSS())
            .pipe(gulp.dest('./css/'))
            .pipe(browserSync.stream());
});



gulp.task('watch', function () {
    gulp.start('default');
    gulp.watch(['*.html'], ['html']);
//    watch('sass/*.scss', function () {
//        gulp.start('css')
//    });
    gulp.watch('sass/*.scss', ['sass']);
    
    watch('scripts/_maximised/modules/*.js', function () {
        gulp.start('js-modules')
    });
    watch('scripts/_maximised/utilities/*.js', function () {
        gulp.start('js-utilities')
    });
    watch('scripts/_maximised/components/*.js', function () {
        gulp.start('js-components')
    });
    watch('scripts/_maximised/scripts/*.js', function () {
        gulp.start('js-scripts')
    });
//    watch('scripts/*.all.js', function () {
//        gulp.start('js-all')
//                .on("change", reload)
//    });
    gulp.watch('scripts/*.all.js', ['js-all']).on("change", reload);
});

//gulp.task('default', function () {
//    return [
//        gulp.start('css'),
//        gulp.start('js'),
//        gulp.start('browser-sync'),
//    ];
//});

// Default Task
gulp.task('default', ['sass', 'js', 'watch', 'browser-sync']);